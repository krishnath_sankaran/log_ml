'''
Created on Jan 17, 2017

@author: sankkri
'''
import json
import pandas
from datatransformation import transform_data
from sklearn.externals import joblib



if __name__ == '__main__':
    
    #the showcase log is already pickled below , so commenting this file
    """
    showcaselogs = []
    
    with open('config.json') as config_file:
        showcaselogs.append(json.load(config_file)["showcasefile"])
        
    dfLogsShowcase = transform_data(showcaselogs)
    
    
    joblib.dump(dfLogsShowcase, 'dfLogsShowcase')
    """
    
    dfLogsShowcase = joblib.load('dfLogsShowcase')
    
    X_test = dfLogsShowcase[["PerLag1", "PerLag2"]]
    
    pred = pandas.DataFrame(index=dfLogsShowcase.index)
    
    
    pred["Direction"] = dfLogsShowcase["Direction"]
    #Loading QDA model from file named 'QDA'
    QDA = joblib.load('QDA')
    pred["QDA"] = QDA.predict(X_test)
    
    #Loading LDA model from file named 'LDA'
    LDA = joblib.load('LDA')
    pred["LDA"] = LDA.predict(X_test)
    
    
    #Loading LR model from file named 'LR'
    LR = joblib.load('LR')
    pred["LR"] = LR.predict(X_test)
    
    print pred
    
    
    