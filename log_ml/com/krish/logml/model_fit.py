'''
Created on Jan 16, 2017

@author: ib121
'''

import numpy
from sklearn.externals import joblib



if __name__ == '__main__':
    pass





def fit_model(name, model, x_train, y_train, x_test, pred):
    """Fits a classification model (for our purposes this is LR, LDA and QDA)
    using the training data, then makes a prediction and subsequent hit rat
    for the test data."""
    
    #Fit  and predict the model on the training and then test data
#     print "before X train"
#     print x_train.to_string()
#     print "before Y train"
#     print y_train.to_string()
    print "y train is "
    print y_train
    model.fit(x_train, y_train)
    
    #Now pickling the model into file system
    joblib.dump(model, name)
    
    pred[name] = model.predict(x_test)
    
    
    
    #Calculate the hit rate based on the direction tested
    pred["%s_Correct" % name] = (1.0 + pred[name]*pred["Actual"])/2.0
    hit_rate = numpy.mean(pred["%s_Correct" % name])
    print "%s: %.3f" % (name, hit_rate)    