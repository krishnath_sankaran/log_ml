'''
Created on Jan 13, 2017

@author: kichu
'''
import json
from datetime import datetime
import pandas
import numpy as np
from collections import OrderedDict
from sklearn.externals import joblib


def transform_data(traininglogs):
    """This function transfers the nasa logs into per day logs count and puts them into a pandas dataframe
        It also creates lag indicators of previous two days count of logs"""
    
    #Create a new Dataframe that holds all the data
    
    #dfLogCount = pandas.DataFrame()

    #An ordered dict maintains the order of insertion
    date_map = OrderedDict()
    
    n =0
    #Read line by line and create count of logs for each day
    for traininglog in traininglogs:
        
        with open(traininglog) as traininglogfile:
            for line in traininglogfile:
                try:
                    current_date =  line.split(' ')[3][1:]
                except:
                    continue            
                current_date_format = datetime.strptime(current_date, '%d/%b/%Y:%H:%M:%S').date()
                #current_count = date_map[str(current_date_format)]  if (date_map[str(current_date_format)] is not None) else 0
                current_count = date_map.get(str(current_date_format),0)
                date_map[str(current_date_format)] = current_count + 1
                #dates.append(str(current_date_format))
                n += 1
                #Stop after 10 million samples, 10 million for train and test, so that we can find out the model that fits best 
                # the last 5 million for testing manually.
                if (n == 15000000):
                    break
        
    print date_map
    
    #Create dataframe
    #dfLogsCount = pandas.DataFrame(index=[str(i) for i in date_map.keys()])
    dfLogsCount = pandas.DataFrame({"dates" : date_map.keys(), "logs":date_map.values()})
    
    print dfLogsCount.index
    
    
    for i in xrange(0, 2):
        dfLogsCount["Lag%s" % str(i+1)] = dfLogsCount["logs"].shift(i+1)
    
    dfLogsCount.fillna(0, inplace=True)    
    
    dfLogsCount["logs%"] = dfLogsCount["logs"].pct_change()*100.0
    dfLogsCount["logs%"].fillna(0, inplace=True)
    

    dfLogsCount["Direction"] = np.sign(dfLogsCount["logs%"])
    dfLogsCount["Direction"].fillna(1.0, inplace=True)
    
    for i in xrange(0, 2):
        dfLogsCount["PerLag%s" % str(i+1)] = dfLogsCount["Lag%s" % str(i+1)].pct_change() * 100.0
    
    
    dfLogsCount = dfLogsCount.replace([np.inf, -np.inf], 0)
    print "after inf replacement"
    print dfLogsCount
    
    dfLogsCount.fillna(0,inplace=True)
    dfLogsCount["Direction"] = dfLogsCount["Direction"].replace(0.0, 1.0)
    
#     for index, row in dfLogsCount.iterrows():
#         #print dfLogsCount.ix[index,"Count"]
#         dfLogsCount[index]["Count"] = date_map.get(str(index))
    
    
    return dfLogsCount    
    
    
            
            
            
            
            
            
            
            
            
            
            
def fnGettransformedData():
    
    
    #Below data is pickled and hence no need to calculate the counts every time
    
    traininglogs = []
    with open('config.json') as config_file:
        properties_file  = json.load(config_file)
        traininglogs.append(properties_file["showcasefile"])
        traininglogs.append(properties_file["trainingfile"])
        print traininglogs
    dfLogsData =  transform_data(traininglogs)
    joblib.dump(dfLogsData,"dfLogsData")
    
        
    dfLogsData  = joblib.load("dfLogsData")

    return dfLogsData
            
    
        


if __name__ == '__main__':
    with open('config.json') as config_file:
        traininglog = json.load(config_file)["trainingfile"]
        print traininglog
    transform_data(traininglog)
