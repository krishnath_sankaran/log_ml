'''
Created on Jan 16, 2017

@author: sankkri
'''
from sklearn.linear_model.logistic import LogisticRegression
from sklearn.lda import LDA
from sklearn.qda import QDA

import pandas

from model_fit import *
from split_data import *



if __name__ == '__main__':
    [X_train, X_test, Y_train, Y_test] = fnSplit(30)
    
    pred = pandas.DataFrame(index=Y_test.index)
    pred["Actual"] = Y_test
    
    models = [("LR", LogisticRegression()), ("LDA", LDA()), ("QDA", QDA())]
    
    for m in models:
        fit_model(m[0], m[1] , X_train, Y_train, X_test, pred)
    
